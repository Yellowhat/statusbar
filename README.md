# statusbar

A custom statusbar, written in `rust`, to be used with `swaybar`

[![pipeline status](https://gitlab.com/yellowhat-labs/statusbar/badges/main/pipeline.svg)](https://gitlab.com/yellowhat-labs/statusbar/-/commits/main)
[![Latest Release](https://gitlab.com/yellowhat-labs/statusbar/-/badges/release.svg)](https://gitlab.com/yellowhat-labs/statusbar/-/releases)

## Table of Contents

<!-- vim-markdown-toc GFM -->

* [Installation](#installation)
* [Features](#features)
* [Other](#other)

<!-- vim-markdown-toc -->

## Installation

Build the binary:

```sh
git clone https://gitlab.com/yellowhat-labs/statusbar
cargo build --release
```

Setup `swaybar` in `~/.config/sway/config`:

```sh
bar {
    ...
    status_command nice -n 19 <PATH>/statusbar
    ...
}
```

## Features

Every second:

* Check for network connection
* Display private ip address
* Display WiFi level (if used)
* Display network volume
* Display CPUs load
* Display CPUs frequencies
* Display CPUs temperatures
* Display FANs rpm
* Display RAM usage
* Display screen brightness
* Display battery charge percentage and power usage
* Display audio level

Every 2 minutes:

* Update weather

On demand:

* Update audio level
* Screen brightness

## Other

This repository contains two other implementations:

* `bash`: [statusbar.bash](scripts/statusbar.bash)
* `python`: [statusbar.py](scripts/statusbar.py)
