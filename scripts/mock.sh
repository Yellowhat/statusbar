#!/usr/bin/env sh
# Clean sources to allow run in CI
set -eu

# Mock
BACKLIGHT=/tmp/sys/class/backlight/
BACKLIGHT0="${BACKLIGHT}/intel_backlight/"
mkdir -p "$BACKLIGHT0"
echo "4794" >"${BACKLIGHT0}/max_brightness"
echo "3836" >"${BACKLIGHT0}/brightness"
BAT=/tmp/sys/class/power_supply
BAT0="${BAT}/BAT0/"
mkdir -p "$BAT0"
echo "Charging" >"${BAT0}/status"
echo "42" >"${BAT0}/capacity"
echo "38001000" >"${BAT0}/power_now"
HWMON=/tmp/sys/class/hwmon/
mkdir -p "${HWMON}/hwmon0"
echo "999" >"${HWMON}/hwmon0/fan1_input"
for i in $(seq 1 4); do
    echo "Core $i" >"$HWMON/hwmon0/temp${i}_label"
    echo "$((40 + i))000" >"$HWMON/hwmon0/temp${i}_input"
done

# wpctl mock
cat >/usr/bin/wpctl <<EOF
#!/usr/bin/env bash
cat <<EOOF
Volume: 0.30
EOOF
EOF
chmod +x /usr/bin/wpctl

# Bash
sed -e "s|while true;|for ((i = 0; i <= $1; i++));|g" \
    -e "s|sleep 1|sleep 0.1|g" \
    scripts/statusbar.bash >/tmp/z.bash

# Python
sed -e "s|while True:|for i in range($1):|g" \
    -e "s|sleep(1)|sleep(0.1)|g" \
    -e "s|if (datetime|#|g" \
    -e "s|Thread(target|#|g" \
    -e "s|/sys/class/backlight|${BACKLIGHT}|g" \
    -e "s|/sys/class/hwmon|${HWMON}|g" \
    -e "s|/sys/class/power_supply|${BAT}|g" \
    scripts/statusbar.py >/tmp/z.py

# Rust
sed -e "s|loop {|for _ in 0..$1 {|g" \
    -e "s|from_secs(1)|from_millis(100)|" \
    -e "s|// std::process::exit(0)|std::process::exit(0)|" \
    -e "s|/sys/class/backlight|${BACKLIGHT}|g" \
    -e "s|self.get_weather();|//|g" \
    -e "s|/sys/class/hwmon|${HWMON}|g" \
    -e "s|/sys/class/power_supply|${BAT}|g" \
    -e "s|self.weather =|// |g" \
    -i src/main.rs
