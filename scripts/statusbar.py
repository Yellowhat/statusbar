#!/usr/bin/env python
"""Statusbar for Sway"""

from datetime import datetime
from glob import glob
from json import load
from signal import signal, SIGUSR1
from socket import socket, AF_INET, SOCK_DGRAM
from subprocess import run
from threading import Thread
from time import sleep
from typing import List, Union
from urllib.request import Request, urlopen


def read_file(path: str) -> Union[str, List[str]]:
    """Helper function to read a file"""
    with open(path, encoding="UTF8") as obj:
        lines = obj.readlines()
    return lines[0].strip() if len(lines) == 1 else lines


def get_cpu_load() -> str:
    """CPU load information"""
    out = read_file("/proc/loadavg")
    return out[: out.rfind(" ")]


def get_cpu_freq() -> str:
    """CPU frequency information"""
    freqs = [
        float(f.split()[-1]) / 1000.0 for f in read_file("/proc/cpuinfo") if "MHz" in f
    ]
    return f"{min(freqs):.1f} {sum(freqs) / len(freqs):.1f} {max(freqs):.1f} GHz"


def get_memory() -> str:
    """RAM usage"""
    lst = read_file("/proc/meminfo")
    tot = int(lst[0].split()[1]) / 1024**2
    ava = int(lst[2].split()[1]) / 1024**2
    return f"{tot - ava:.2f} / {tot:.2f} GB"


def get_battery() -> str:
    """Battery usage"""
    status = read_file("/sys/class/power_supply/BAT0/status")
    capacity = read_file("/sys/class/power_supply/BAT0/capacity")
    icons = ["❗", "▁", "▂", "▃", "▄", "▅", "▆", "▇", "█"]
    icon = icons[int(int(capacity) / 100 * len(icons)) % len(icons)]
    dct = {
        "Charging": ["lightgreen", "🔌"],
        "Discharging": ["orange", icon],
        "Full": ["white", "⚡"],
        "Not charging": ["white", "🛑"],
        "Unknown": ["white", "🤷"],
    }
    power = float(read_file("/sys/class/power_supply/BAT0/power_now")) / 10**6
    return f"<span foreground='{dct[status][0]}'>{dct[status][1]} {capacity}%</span> - {power:.1f} W"


def get_time() -> str:
    """Date/Time"""
    return datetime.now().strftime("%a %Y-%m-%d %H:%M:%S")


class Status:
    """Display system status"""

    def __init__(self) -> None:
        # Search hwmon* path
        hwmon_dir = "/sys/class/hwmon"
        self.fans = glob(f"{hwmon_dir}/*/fan*")
        self.cpu_temps = [
            f.replace("_label", "_input")
            for f in glob(f"{hwmon_dir}/*/temp*_label")
            if "Core" in read_file(f)
        ]
        self.ping = 0
        self.weather = "🌞"
        self.screen_lum = ""
        self.audio = ""

    def get_ping(self) -> None:
        """Run ping"""
        out = run(
            ["ping", "-w", "10", "-c", "4", "8.8.8.8"],
            capture_output=True,
            check=False,
        ).stdout.decode("UTF8")
        self.ping = int(float(out.split("=")[-1].split("/")[1]))

    def get_weather(self) -> None:
        """Update weather information"""
        url = "https://api.met.no/weatherapi/locationforecast/2.0/compact?lat=50.07&lon=8.23"
        icons = {
            "clearsky": "☀️",
            "cloudy": "☁️",
            "fair": "⛅️",
            "fog": "🌫",
            "heavyrain": "☔️",
            "heavyrainandthunder": "☔️",
            "heavyrainshowers": "☔️",
            "heavyrainshowersandthunder": "☔️",
            "heavysleet": "🌦",
            "heavysleetandthunder": "🌦",
            "heavysleetshowers": "🌦",
            "heavysleetshowersandthunder": "🌦",
            "heavysnow": "❄️",
            "heavysnowandthunder": "❄️",
            "heavysnowshowers": "❄️",
            "heavysnowshowersandthunder": "❄️",
            "lightrain": "🌧",
            "lightrainandthunder": "🌧",
            "lightrainshowers": "🌧",
            "lightrainshowersandthunder": "🌧",
            "lightsleet": "🌧",
            "lightsleetandthunder": "🌧",
            "lightsleetshowers": "🌧",
            "lightsnow": "❄️",
            "lightsnowandthunder": "❄️",
            "lightsnowshowers": "❄️",
            "lightssleetshowersandthunder": "❄️",
            "lightssnowshowersandthunder": "❄️",
            "partlycloudy": "☁️",
            "rain": "🌧",
            "rainandthunder": "🌧",
            "rainshowers": "🌧",
            "rainshowersandthunder": "🌧",
            "sleet": "🌧",
            "sleetandthunder": "🌧",
            "sleetshowers": "🌧",
            "sleetshowersandthunder": "🌧",
            "snow": "❄️",
            "snowandthunder": "❄️",
            "snowshowers": "❄️",
            "snowshowersandthunder": "❄️",
        }
        req = Request(url)
        req.add_header(
            "User-Agent",
            "Mozilla/5.0 (Wayland; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0",
        )
        with urlopen(req) as obj:  # nosemgrep
            response = load(obj)
        details = response["properties"]["timeseries"][0]["data"]["instant"]["details"]
        temp = details["air_temperature"]
        humi = details["relative_humidity"]
        press = details["air_pressure_at_sea_level"]
        cloud = details["cloud_area_fraction"]
        wind = details["wind_speed"]
        symbol = response["properties"]["timeseries"][0]["data"]["next_1_hours"][
            "summary"
        ]["symbol_code"]
        symbol = symbol.split("_")[0]
        icon = icons[symbol].encode("UTF16", "surrogatepass").decode("UTF16")
        self.weather = (
            f"{icon} {temp:.1f}°C {humi:.0f}% {press:.0f}hPa {cloud:.0f}% {wind:.1f}m/s"
        )

    def get_net(self) -> str:
        """Network information"""
        try:
            with socket(AF_INET, SOCK_DGRAM) as skt:
                skt.connect(("10.255.255.255", 1))
                ip_prv = skt.getsockname()[0]
        except OSError:
            return "<span foreground='red'>No connection</span>"
        tx_bytes = sum(
            int(read_file(f)) for f in glob("/sys/class/net/[e,w]*/statistics/tx_bytes")
        )
        rx_bytes = sum(
            int(read_file(f)) for f in glob("/sys/class/net/[e,w]*/statistics/rx_bytes")
        )
        try:
            wifi_level = read_file("/proc/net/wireless")[2].split()[3][:-1]
            wifi = f"{wifi_level} dBA"
        except (FileNotFoundError, IndexError):
            wifi = ""
        if (datetime.now().second % 30) == 0:
            Thread(target=self.get_ping).start()
        if (datetime.now().second % 60) == 0 and (datetime.now().minute % 10) == 0:
            Thread(target=self.get_weather).start()
        return f"{wifi} {ip_prv} ↑ {tx_bytes / 1024 / 1024:.1f} ↓ {rx_bytes / 1024 / 1024:.1f} MB 🏓 {self.ping} ms"

    def get_cpu_temp(self) -> str:
        """CPU temperature information"""
        degs = [float(read_file(f)[:-3]) for f in self.cpu_temps]
        return f"{min(degs):.0f} {sum(degs) / len(degs):.0f} {max(degs):.0f} °C"

    def get_fan(self) -> str:
        """Fan speed"""
        rpm = " - ".join(read_file(f) for f in self.fans)
        return f"{rpm} RPM"

    def get_screen_lum(self) -> None:
        """Screen brightness"""
        lum_max = float(
            read_file("/sys/class/backlight/intel_backlight/max_brightness")
        )
        lum_cur = float(read_file("/sys/class/backlight/intel_backlight/brightness"))
        self.screen_lum = f"🔆{lum_cur / lum_max * 100:.0f}%"

    def get_audio(self) -> None:
        """Audio volume"""
        icons = ["🔈", "🔉", "🔊"]
        out = run(
            ["wpctl", "get-volume", "@DEFAULT_AUDIO_SINK@"],
            capture_output=True,
            check=False,
        ).stdout.decode("UTF8")
        vol = float(out.split()[1]) * 100.0
        icon = icons[-1 if vol >= 100 else int(vol / 100 * len(icons)) % len(icons)]
        self.audio = "🔇    " if ("MUTED" in out or vol == 0) else f"{icon}{vol:3.0f}%"

    def show(self) -> None:
        """Print"""
        print(
            f"{self.get_net()} | {self.weather} | {get_cpu_load()} - {get_cpu_freq()} - {self.get_cpu_temp()} | {self.get_fan()} | {get_memory()} | {self.screen_lum} | {get_battery()} | {self.audio} | {get_time()}",
            flush=True,
        )

    def handle_signal(self, *_) -> None:
        """Run on external signal"""
        # pylint: disable=unused-argument
        self.get_screen_lum()
        self.get_audio()
        self.show()

    def main(self) -> None:
        """Mainloop"""
        sleep(1)
        self.handle_signal()
        signal(SIGUSR1, self.handle_signal)

        while True:
            sleep(1)
            self.show()


if __name__ == "__main__":
    loop = Status()
    loop.main()
