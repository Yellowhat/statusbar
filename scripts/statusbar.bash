#!/usr/bin/env bash
set -euo pipefail

update_cpu() {
    cpu_load=$(cut -d ' ' -f 1-3 </proc/loadavg)
    cpu_freq=$(awk '/MHz/ {printf "%.2f ", $4 / 1000}' /proc/cpuinfo)
}

update_memory() {
    memory=$(free -m | awk 'FNR == 2 {printf "%.2f/%.2f GB", $3 / 1024,  $2 / 1024 }')
}

update_time() {
    time=$(date "+%a %Y-%m-%d %H:%M:%S")
}

display() {
    printf "%s\n" "$cpu_load - $cpu_freq | fan | $memory | lum | bat | audio | $time"
}

while true; do
    sleep 1 &
    wait && {
        update_cpu
        update_memory
        update_time

        display
    }
done
