#!/usr/bin/env bash
# Update version
set -euo pipefail

VERSION=$1

echo "[INFO] Current:"
grep "^version" Cargo.toml

sed -e "s|^version =.*|version = \"$VERSION\"|" \
    -i Cargo.toml

echo "[INFO] Replaced:"
grep "^version" Cargo.toml
