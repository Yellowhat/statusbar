#!/usr/bin/env bash
# Run cachegrind
set -xeuo pipefail

# Set PYTHONHASHSEED to a fixed value to reduce variability for python
export PYTHONHASHSEED=123456789

# Set cache configuration simulated (cache size, associativity and line size) for 3 levels:
# | --I1 | Level 1 instruction cache  | 256 kB | 256*1024      |  262144 B |
# | --D1 | Level 1 data cache         | 256 kB | 256*1024      |  262144 B |
# | --LL | Last-Level (Level 3) cache |   8 MB |   8*1024*1024 | 8388608 B |
# Enable collection of cache access and miss counts
# Enable collection of branch instruction and misprediction counts
# Collect spawn child processes
echo "DURATION: $DURATION"
valgrind \
    --tool=cachegrind \
    --I1=262144,8,64 \
    --D1=262144,8,64 \
    --LL=8388608,16,64 \
    --time-stamp=yes \
    --cache-sim=yes \
    --branch-sim=yes \
    --trace-children=yes \
    --cachegrind-out-file=/tmp/cachegrind \
    "$@"

# Computer readable results
grep -e "events\|summary" /tmp/cachegrind
