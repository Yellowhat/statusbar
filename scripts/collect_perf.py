#!/usr/bin/env python
"""
Collect performance metrics

Requires an environment variables:
- `CI_API_V4_URL`: GitLab API url
- `CI_PROJECT_ID`: GitLab project id
- `READ_API_TOKEN`: Access Token with:
  - role: Guest
  - scopes:
      - read_api
The default `CI_JOB_TOKEN` is not enough.
"""

import re
from concurrent.futures import ThreadPoolExecutor
from json import load
from os import environ
from typing import Any
from urllib.request import Request, urlopen
import pandas as pd


def get(url_add: str, raw: bool = False) -> Any:
    """Make request"""

    url_base = f"{environ['CI_API_V4_URL']}/projects/{environ['CI_PROJECT_ID']}"
    req = Request(f"{url_base}/{url_add}")
    req.add_header("PRIVATE-TOKEN", environ["READ_API_TOKEN"])
    with urlopen(req) as obj:  # nosec # nosemgrep
        if raw:
            return obj.read().decode("UTF8")
        return load(obj)


def parse_pipeline(pipeline: dict[str, str]) -> Any:
    """Parse pipeline"""

    pipeline_id = pipeline["id"]
    pipeline_ref = pipeline["ref"]
    print(f"[INFO] Parsing | {pipeline_ref:8s} | {pipeline_id}")

    results = []
    jobs = get(f"/pipelines/{pipeline_id}/jobs?per_page=100")
    for job in jobs:
        if "valgrind" not in job["name"]:
            continue

        keys = values = []
        lines = get(f"jobs/{job['id']}/trace", raw=True)
        for line in lines.splitlines():
            if line.startswith("events: "):
                keys = line.split()[1:]
            if line.startswith("summary: "):
                values = list(map(int, line.split()[1:]))

        metrics = dict(zip(keys, values))
        metrics["RAM"] = metrics["DLmr"] + metrics["DLmw"] + metrics["ILmr"]
        metrics["L3"] = (
            metrics["I1mr"] + metrics["D1mw"] + metrics["D1mr"] - metrics["RAM"]
        )
        metrics["Total_Memory"] = metrics["Ir"] + metrics["Dr"] + metrics["Dw"]
        metrics["L1"] = metrics["Total_Memory"] - metrics["L3"] - metrics["RAM"]
        metrics["perf"] = metrics["L1"] + (5 * metrics["L3"]) + (35 * metrics["RAM"])

        dct = {
            "ref": pipeline_ref,
            "date": job["commit"]["created_at"],
            "duration": int(re.search(r"\[(.*?)\]", job["name"]).group(1).split(", ")[-1]),  # type: ignore
            **metrics,
        }
        if regex := re.search(r"(Python \S+)", lines):
            dct["version"] = regex.group(1)
        elif regex := re.search(r"(bash, version \S+)", lines):
            dct["version"] = regex.group(1)
        elif regex := re.search(r"(rustc \d\S+)", lines):
            dct["version"] = regex.group(1)
        results.append(dct)

    return results


if __name__ == "__main__":
    pipelines = get("pipelines?per_page=100")
    with ThreadPoolExecutor(max_workers=10) as pool:
        out = pool.map(parse_pipeline, pipelines)

    df = (
        pd.DataFrame(sum(out, []))
        .set_index(["date", "ref", "version", "duration"])
        .sort_index()
    )
    perf = df.groupby(["date", "ref", "version"])["perf"].transform(
        lambda g: g - g.iat[0]
    )
    df.sort_values(by=["date"], inplace=True)
    df["perf/sec"] = perf / df.index.get_level_values("duration")
    pd.options.display.float_format = "{:,.0f}".format
    print(df)
    df.to_csv("perf.csv")

    # Show per language
    df["language"] = df.index.get_level_values(2).str.split().str[0]
    for lang, dff in df.groupby("language"):
        print(dff["perf/sec"])
        print("-" * 120)
