.DEFAULT_GOAL := help
SHELL          = /usr/bin/env bash -o pipefail
.SHELLFLAGS    = -ec
# renovate: datasource=docker depName=docker.io/rust
RUST_VER       = 1.85.0

.PHONY: it
it: ## Start interactive go container
	podman run \
		--interactive \
		--tty \
		--rm \
		--volume "${PWD}:/data:z" \
		--workdir /data \
		--entrypoint bash \
		"docker.io/rust:$(RUST_VER)" -c \
		"printf \"#!/usr/bin/env bash\\\necho Volume: 0.30\" >/usr/local/bin/wpctl && chmod +x /usr/local/bin/wpctl && bash"

.PHONY: help
help: ## Makefile Help Page
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n\nTargets:\n"} /^[\/\%a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-21s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST) 2>/dev/null
