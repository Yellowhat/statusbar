// https://semantic-release.gitbook.io/semantic-release/usage/configuration
module.exports = {
    plugins: [
        [
            "@semantic-release/git",
            {
                message: "chore(release): ${nextRelease.version}",
                assets: ["CHANGELOG.md", "Cargo.toml"],
            },
        ],
        [
            "@semantic-release/exec",
            {
                prepareCmd:
                    "sh scripts/update_version.sh ${nextRelease.version}",
            },
        ],
    ],
};
