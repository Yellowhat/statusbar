use crate::cpu::get_cpu_freq;
use crate::cpu::get_cpu_load;
use crate::cpu::get_cpu_temp;
use std::fs::File;
use std::io::Write;

#[test]
fn test_cpu_freq() {
    let freq = get_cpu_freq();
    let lst = freq.split_whitespace().collect::<Vec<&str>>();
    assert_eq!(lst.len(), 4);

    assert!(lst[0].parse::<f64>().is_ok());
    assert!(lst[1].parse::<f64>().is_ok());
    assert!(lst[2].parse::<f64>().is_ok());

    assert_eq!(lst[3], "GHz");
}

#[test]
fn test_cpu_load() {
    let load = get_cpu_load();
    let lst = load.split_whitespace().collect::<Vec<&str>>();
    assert_eq!(lst.len(), 4);

    assert!(lst[0].parse::<f64>().is_ok());
    assert!(lst[1].parse::<f64>().is_ok());
    assert!(lst[2].parse::<f64>().is_ok());

    assert!(lst[3].contains('/'));

    let proc = lst[3].split('/').collect::<Vec<&str>>();
    assert!(proc[0].parse::<i64>().is_ok());
    assert!(proc[1].parse::<i64>().is_ok());
}

#[test]
fn test_cpu_temp() {
    let mut file0 = File::create("/tmp/cpu_temp_0").expect("Unable to create file");
    file0.write_all(b"40000").expect("Unable to write data");
    let mut file1 = File::create("/tmp/cpu_temp_1").expect("Unable to create file");
    file1.write_all(b"55000").expect("Unable to write data");
    let mut file2 = File::create("/tmp/cpu_temp_2").expect("Unable to create file");
    file2.write_all(b"60000").expect("Unable to write data");

    let cpu_temp_paths = vec![
        "/tmp/cpu_temp_0".to_string(),
        "/tmp/cpu_temp_1".to_string(),
        "/tmp/cpu_temp_2".to_string(),
    ];

    let result = get_cpu_temp(cpu_temp_paths);
    assert_eq!(result, "40 52 60 °C");
}
