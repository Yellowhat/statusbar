use crate::other::get_datetime;
use crate::other::get_fan;
use crate::other::get_screen_lum;
use crate::other::get_weather;
use chrono::Local;
use std::fs::File;
use std::fs::create_dir_all;
use std::io::Write;
use std::path::Path;

#[test]
fn test_datetime() {
    let datetime = get_datetime();
    let lst = datetime.split_whitespace().collect::<Vec<&str>>();
    let now = Local::now();

    assert_eq!(lst[0], now.format("%a").to_string());
    assert_eq!(lst[1], now.format("%Y-%m-%d").to_string());
    assert_eq!(lst[2], now.format("%H:%M:%S").to_string());
}

#[test]
fn test_fan() {
    let fan_dir = Path::new("/tmp/fan");
    if !fan_dir.exists() {
        create_dir_all(fan_dir).unwrap();
    }

    let fan0 = fan_dir.join("fan0_input");
    let mut f = File::create(fan0.clone()).expect("Unable to create file");
    f.write_all(b"999").expect("Unable to write data");

    let fan1 = fan_dir.join("fan1_input");
    let mut f = File::create(fan1.clone()).expect("Unable to create file");
    f.write_all(b"1009").expect("Unable to write data");

    let rpms = get_fan(vec![fan0, fan1]);
    assert_eq!(rpms, "999 1009 RPM");
}

#[test]
fn test_screen_lum() {
    let screen_dir = Path::new("/tmp/backlight");
    if !screen_dir.exists() {
        create_dir_all(screen_dir).unwrap();
    }

    let mut f = File::create(screen_dir.join("max_brightness")).expect("Unable to create file");
    f.write_all(b"255").expect("Unable to write data");

    let mut f = File::create(screen_dir.join("brightness")).expect("Unable to create file");
    f.write_all(b"93").expect("Unable to write data");

    let screen_lum = get_screen_lum(vec![screen_dir.to_path_buf()]);
    assert_eq!(screen_lum, "🔆36%");
}

#[test]
fn test_get_weather() {
    let _weather = get_weather();
}
