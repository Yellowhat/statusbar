use std::fs;
#[cfg(test)]
#[path = "./memory_test.rs"]
mod memory_test;

pub fn get() -> String {
    let data = fs::read_to_string("/proc/meminfo").expect("Error meminfo");
    let mem_total = data
        .lines()
        .next()
        .expect("No memory total")
        .split_whitespace()
        .collect::<Vec<&str>>()[1]
        .parse::<f32>()
        .unwrap()
        / 1024.0
        / 1024.0;
    let mem_avail = data
        .lines()
        .nth(2)
        .expect("No memory available")
        .split_whitespace()
        .collect::<Vec<&str>>()[1]
        .parse::<f32>()
        .unwrap()
        / 1024.0
        / 1024.0;
    format!("{:.2}/{mem_total:.2} GB", mem_total - mem_avail)
}
