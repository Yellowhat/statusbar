use crate::memory::get;

#[test]
fn test_memory() {
    let load = get();
    let lst = load.split_whitespace().collect::<Vec<&str>>();
    assert_eq!(lst.len(), 2);

    let mem = lst[0].split('/').collect::<Vec<&str>>();
    assert!(mem[0].parse::<f64>().is_ok());
    assert!(mem[1].parse::<f64>().is_ok());

    assert_eq!(lst[1], "GB");
}
