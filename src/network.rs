use std::fs;
#[cfg(test)]
#[path = "./network_test.rs"]
mod network_test;

pub fn get_ip() -> Option<std::net::IpAddr> {
    let Ok(socket) = std::net::UdpSocket::bind("0.0.0.0:0") else {
        return None;
    };
    match socket.connect("10.255.255.255:1") {
        Ok(()) => (),
        Err(_) => return None,
    };
    match socket.local_addr() {
        Ok(addr) => Some(addr.ip()),
        Err(_) => None,
    }
}

pub fn get_wifi() -> Option<String> {
    let file = fs::read_to_string("/proc/net/wireless").ok()?;
    let level = file.lines().nth(2)?;
    let value = level.split_whitespace().nth(3)?.parse::<f32>().ok()?;
    Some(format!("{value:.0} dBA "))
}
