use std::fmt::Write as _;
use std::fs;
use std::path::Path;
use std::path::PathBuf;
#[cfg(test)]
#[path = "./battery_test.rs"]
mod battery_test;

fn get_power(battery: &Path) -> f32 {
    fs::read_to_string(battery.display().to_string() + "/power_now")
        .expect("Error BAT/power_now")
        .trim()
        .parse::<f32>()
        .unwrap()
        / f32::powi(10.0, 6)
}

pub fn get(batteries: Vec<PathBuf>) -> String {
    let icons = [
        "\u{2757}", "\u{2581}", "\u{2582}", "\u{2583}", "\u{2584}", "\u{2585}", "\u{2586}",
        "\u{2587}", "\u{2588}",
    ]; // ["❗", "▁", "▂", "▃", "▄", "▅", "▆", "▇", "█"]
    let n_icons = icons.len() as f32;

    let mut charges = String::new();
    for battery in batteries {
        let capacity = fs::read_to_string(battery.as_path().display().to_string() + "/capacity")
            .expect("Error BAT/capacity")
            .trim()
            .parse::<f32>()
            .unwrap();
        let index = (capacity / 100.0 * n_icons) % n_icons;
        let icon = icons[index as usize];
        let status = fs::read_to_string(battery.as_path().display().to_string() + "/status")
            .expect("Error BAT/status");
        let color = match status.trim() {
            "Charging" => "lightgreen",
            "Discharging" => "orange",
            _ => "white",
        };
        let emoji = match status.trim() {
            "Charging" => "\u{1f50c}", // 🔌
            "Discharging" => icon,
            "Full" => "\u{26a1}",          // ⚡
            "Not charging" => "\u{1f6d1}", // 🛑
            _ => "\u{1f937}",              // 🤷
        };
        let power = get_power(&battery);

        _ = write!(
            charges,
            "<span foreground='{color}'>{emoji} {capacity:2}%</span> - {power:.1} W |"
        );
    }
    charges
}
