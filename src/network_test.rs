use crate::network::get_ip;

#[test]
fn test_get_ip() {
    let ip = get_ip();
    assert!(ip.is_some());
    assert!(ip.unwrap().is_ipv4());
}
