use std::fs;
#[cfg(test)]
#[path = "./cpu_test.rs"]
mod cpu_test;

pub fn get_cpu_load() -> String {
    // load average over 1, 5, and 15 minutes | number of currently executing kernel scheduling entities (processes, threads)/number of kernel scheduling entities that currently exist on the system
    let out = fs::read_to_string("/proc/loadavg").expect("Error /proc/loadavg");
    let idx = out.rfind(' ').unwrap();
    out[..idx].to_string()
}

pub fn get_cpu_freq() -> String {
    let data = fs::read_to_string("/proc/cpuinfo").expect("Error cpuinfo");
    let mut freqs = Vec::new();
    for line in data.lines() {
        if line.starts_with("cpu MHz") {
            let freq = line
                .split_whitespace()
                // Expect 4 tokens - 'cpu', 'mhz', ':', <value>
                .collect::<Vec<&str>>()[3]
                .parse::<f32>()
                .unwrap()
                / 1000.0;
            freqs.push(freq);
        }
    }
    let min: f32 = freqs.iter().copied().fold(1. / 0., f32::min); // +infinite and min
    let max: f32 = freqs.iter().copied().fold(-1. / 0., f32::max); // -infinite and max
    let mean: f32 = freqs.iter().sum::<f32>() / freqs.len() as f32;
    format!("{min:.1} {mean:.1} {max:.1} GHz")
}

pub fn get_cpu_temp(cpu_temp_paths: Vec<String>) -> String {
    let mut temps = Vec::new();
    for path in cpu_temp_paths {
        let temp = fs::read_to_string(path)
            .expect("Error cputemp")
            .trim()
            .parse::<f32>()
            .unwrap()
            / 1000.0;
        temps.push(temp);
    }
    let min: f32 = temps.iter().copied().fold(1. / 0., f32::min); // +infinite and min
    let max: f32 = temps.iter().copied().fold(-1. / 0., f32::max); // -infinite and max
    let mean: f32 = temps.iter().sum::<f32>() / temps.len() as f32;
    format!("{min:.0} {mean:.0} {max:.0} \u{b0}C") // °
}
