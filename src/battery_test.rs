use crate::battery::get;
use crate::battery::get_power;
use std::fs::File;
use std::fs::create_dir_all;
use std::io::Write;
use std::path::Path;

#[test]
fn test_battery_power() {
    let power = 33.2;

    let bat_dir = Path::new("/tmp/BAT0_power");
    if !bat_dir.exists() {
        create_dir_all(bat_dir).unwrap();
    }

    let power_now = power * f32::powi(10.0, 6);
    let mut f = File::create(bat_dir.join("power_now")).expect("Unable to create file");
    f.write_all(power_now.to_string().as_bytes())
        .expect("Unable to write data");

    let output = get_power(bat_dir);
    assert!((power - output).abs() < 0.00001);
}

#[test]
fn test_battery() {
    let test_cases = [
        (
            88,
            1.1,
            "Charging",
            "<span foreground='lightgreen'>🔌 88%</span> - 1.1 W |",
        ),
        (
            10,
            1.0,
            "Discharging",
            "<span foreground='orange'>❗ 10%</span> - 1.0 W |",
        ),
        (
            5,
            33.1,
            "Full",
            "<span foreground='white'>⚡  5%</span> - 33.1 W |",
        ),
        (
            10,
            0.0,
            "Not charging",
            "<span foreground='white'>🛑 10%</span> - 0.0 W |",
        ),
    ];

    let bat_dir = Path::new("/tmp/BAT0");
    if !bat_dir.exists() {
        create_dir_all(bat_dir).unwrap();
    }

    for (capacity, power, status, want_output) in test_cases {
        let mut f = File::create(bat_dir.join("capacity")).expect("Unable to create file");
        f.write_all(capacity.to_string().as_bytes())
            .expect("Unable to write data");

        let power_now = power * f32::powi(10.0, 6);
        let mut f = File::create(bat_dir.join("power_now")).expect("Unable to create file");
        f.write_all(power_now.to_string().as_bytes())
            .expect("Unable to write data");

        let mut f = File::create(bat_dir.join("status")).expect("Unable to create file");
        f.write_all(status.as_bytes())
            .expect("Unable to write data");

        let output = get(vec![bat_dir.to_path_buf()]);
        assert_eq!(output, want_output);
    }
}
