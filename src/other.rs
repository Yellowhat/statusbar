use chrono::Local;
use serde_json::Value;
use std::fmt::Write as _;
use std::fs;
use std::path::PathBuf;
#[cfg(test)]
#[path = "./other_test.rs"]
mod other_test;

pub fn get_datetime() -> String {
    Local::now().format("%a %Y-%m-%d %H:%M:%S").to_string()
}

pub fn get_fan(fans: Vec<PathBuf>) -> String {
    let mut rpms = String::new();
    for fan in fans {
        let rpm = fs::read_to_string(fan)
            .expect("Error fan")
            .trim()
            .parse::<f32>()
            .unwrap();
        _ = write!(rpms, "{rpm:.0} ");
    }
    _ = write!(rpms, "RPM");
    rpms
}

pub fn get_weather() -> Option<String> {
    let url = "https://api.met.no/weatherapi/locationforecast/2.0/compact?lat=50.07&lon=8.23";
    let mut handle = curl::easy::Easy::new();
    let mut headers = curl::easy::List::new();
    headers
        .append(
            "User-Agent:Mozilla/5.0 (Wayland; Linux x86_64; rv:108.0) Gecko/20100101 Firefox/108.0",
        )
        .unwrap();
    handle.http_headers(headers).unwrap();
    handle.url(url).unwrap();
    let mut data = Vec::new();
    {
        let mut transfer = handle.transfer();
        transfer
            .write_function(|new_data| {
                data.extend_from_slice(new_data);
                Ok(new_data.len())
            })
            .unwrap();
        transfer.perform().ok()?;
    }
    let json: Value = serde_json::from_slice(&data).ok()?;

    let symbol_code =
        json["properties"]["timeseries"][0]["data"]["next_1_hours"]["summary"]["symbol_code"]
            .as_str()
            .unwrap();
    let symbol = symbol_code.split('_').next().unwrap();
    let icon = match symbol {
        "clearsky" => "\u{2600}\u{fe0f}",                // ☀️
        "cloudy" | "partlycloudy" => "\u{2601}\u{fe0f}", // ☁️
        "fair" => "\u{26c5}\u{fe0f}",                    // ⛅️
        "fog" => "\u{1f32b}",                            // 🌫
        "heavyrain" | "heavyrainandthunder" | "heavyrainshowers" | "heavyrainshowersandthunder" => {
            "\u{2614}\u{fe0f}" // ☔️
        }
        "heavysleet"
        | "heavysleetandthunder"
        | "heavysleetshowers"
        | "heavysleetshowersandthunder" => "\u{1F326}\u{FE0F}", // 🌦
        "heavysnow"
        | "heavysnowandthunder"
        | "heavysnowshowers"
        | "heavysnowshowersandthunder"
        | "lightsnow"
        | "lightsnowandthunder"
        | "lightsnowshowers"
        | "lightssleetshowersandthunder"
        | "lightssnowshowersandthunder"
        | "snow"
        | "snowandthunder"
        | "snowshowers"
        | "snowshowersandthunder" => "\u{2744}\u{fe0f}", // ❄️
        "lightrain"
        | "lightrainandthunder"
        | "lightrainshowers"
        | "lightrainshowersandthunder"
        | "lightsleet"
        | "lightsleetandthunder"
        | "lightsleetshowers"
        | "rain"
        | "rainandthunder"
        | "rainshowers"
        | "rainshowersandthunder"
        | "sleet"
        | "sleetandthunder"
        | "sleetshowers"
        | "sleetshowersandthunder" => "\u{1F327}", // 🌧
        _ => "\u{1f937}", // 🤷
    };
    let details = &json["properties"]["timeseries"][0]["data"]["instant"]["details"];
    Some(format!(
        "{} {:.1}\u{b0}C {:.0}% {:.0}hPa {:.0}% {:.1}m/s", // °
        icon,
        details["air_temperature"].as_f64().unwrap(),
        details["relative_humidity"].as_f64().unwrap(),
        details["air_pressure_at_sea_level"].as_f64().unwrap(),
        details["cloud_area_fraction"].as_f64().unwrap(),
        details["wind_speed"].as_f64().unwrap(),
    ))
}

pub fn get_screen_lum(screen_lum_paths: Vec<PathBuf>) -> String {
    let mut screen_lum = String::new();
    for screen_lum_path in screen_lum_paths {
        let lum_max =
            fs::read_to_string(screen_lum_path.as_path().display().to_string() + "/max_brightness")
                .expect("Error max_brightness")
                .trim()
                .parse::<f32>()
                .unwrap();
        let lum_cur =
            fs::read_to_string(screen_lum_path.as_path().display().to_string() + "/brightness")
                .expect("Error brightness")
                .trim()
                .parse::<f32>()
                .unwrap();
        _ = write!(screen_lum, "\u{1f506}{:.0}%", lum_cur / lum_max * 100.0);
    }
    screen_lum
    // 🔆
}
