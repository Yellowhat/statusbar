use chrono::Local;
use glob::glob;
use signal_hook::consts::signal::SIGUSR1;
use signal_hook::iterator::SignalsInfo;
use signal_hook::iterator::exfiltrator::WithOrigin;
use std::fs;
use std::path::PathBuf;
use std::process::Command;
use std::sync::Arc;
use std::sync::Mutex;
mod battery;
mod cpu;
mod memory;
mod network;
mod other;

struct Status {
    cpu_temps: Vec<String>,
    batteries: Vec<PathBuf>,
    fans: Vec<PathBuf>,
    screen_lum_paths: Vec<PathBuf>,
    audio: String,
    screen_lum: String,
    weather: String,
}

impl Status {
    pub fn new() -> Status {
        let mut cpu_temps = Vec::new();
        let name_temps = ["Core", "CPU"];
        for entry in glob("/sys/class/hwmon/*/temp*_label").expect("Error temp*_label") {
            let path = entry.as_ref().unwrap();
            let line = fs::read_to_string(path).expect("Error");
            if name_temps.iter().any(|c| line.contains(c)) {
                cpu_temps.push(path.to_string_lossy().replace("_label", "_input"));
            }
        }

        let mut batteries = Vec::new();
        for entry in glob("/sys/class/power_supply/BAT*").expect("Error battery*") {
            batteries.push(entry.unwrap());
        }

        let mut fans = Vec::new();
        for entry in glob("/sys/class/hwmon/*/fan1_input").expect("Error fan*") {
            fans.push(entry.unwrap());
        }

        let mut screen_lum_paths = Vec::new();
        for entry in glob("/sys/class/backlight/*").expect("Error screen_lum_paths*") {
            screen_lum_paths.push(entry.unwrap());
        }

        Status {
            cpu_temps,
            batteries,
            fans,
            screen_lum_paths,
            audio: String::new(),
            screen_lum: String::new(),
            weather: "\u{1f31e}".to_string(), // 🌞
        }
    }

    fn get_net(&mut self) -> String {
        let Some(ip) = network::get_ip() else {
            return "<span foreground='red'>No connection</span>".to_string();
        };

        let mut tx_bytes = 0.0;
        for entry in glob("/sys/class/net/[e,w]*/statistics/tx_bytes").expect("Error tx_bytes") {
            let path = entry.as_ref().unwrap();
            let tx = fs::read_to_string(path)
                .expect("Error")
                .trim()
                .parse::<f32>()
                .unwrap();
            tx_bytes += tx;
        }
        tx_bytes = tx_bytes / 1024.0 / 1024.0;

        let mut rx_bytes = 0.0;
        for entry in glob("/sys/class/net/[e,w]*/statistics/rx_bytes").expect("Error rx_bytes") {
            let path = entry.as_ref().unwrap();
            let rx = fs::read_to_string(path)
                .expect("Error rx_bytes")
                .trim()
                .parse::<f32>()
                .unwrap();
            rx_bytes += rx;
        }
        rx_bytes = rx_bytes / 1024.0 / 1024.0;

        if Local::now().timestamp() % 600 == 0 {
            self.weather = other::get_weather().unwrap_or_default();
        };

        let wifi = network::get_wifi().unwrap_or_default();

        format!(
            "{}{:?} \u{2191}{:.1} \u{2193}{:.1} MB | {}", // ↑ ↓
            wifi, ip, tx_bytes, rx_bytes, self.weather,
        )
    }

    fn get_audio(&mut self) {
        let icons = ["\u{1f508}", "\u{1f509}", "\u{1f50a}"]; // ["🔈", "🔉", "🔊"]
        let n_icons = icons.len() as f32;
        let cmd = Command::new("wpctl")
            .args(["get-volume", "@DEFAULT_AUDIO_SINK@"])
            .output()
            .expect("Failed to execute wpctl");
        let out = String::from_utf8(cmd.stdout).unwrap();
        let vol = out
            // Volume: 0.60 [MUTED]
            .split_whitespace()
            .collect::<Vec<&str>>()[1]
            .parse::<f32>()
            .unwrap()
            * 100.0;
        let index = if vol >= 100.0 {
            n_icons - 1.0
        } else {
            (vol / 100.0 * n_icons) % n_icons
        };
        let icon = icons[index as usize];
        self.audio = format!("{icon}{vol:.0}%");
    }

    fn show(&mut self) {
        println!(
            "{net} | {cpu_load} - {cpu_freq} - {cpu_temp} | {fan} | {mem} | {lum} | {bat} {audio} | {date}",
            net = self.get_net(),
            cpu_load = cpu::get_cpu_load(),
            cpu_freq = cpu::get_cpu_freq(),
            cpu_temp = cpu::get_cpu_temp(self.cpu_temps.clone()),
            fan = other::get_fan(self.fans.clone()),
            mem = memory::get(),
            lum = self.screen_lum,
            bat = battery::get(self.batteries.clone()),
            audio = self.audio,
            date = other::get_datetime()
        );
    }

    fn handle_signal(&mut self) {
        self.get_audio();
        self.screen_lum = other::get_screen_lum(self.screen_lum_paths.clone());
        self.show();
    }
}

struct StatusLoop {
    inner: Arc<Mutex<Status>>,
}

impl StatusLoop {
    fn new() -> StatusLoop {
        StatusLoop {
            inner: Arc::new(Mutex::new(Status::new())),
        }
    }

    fn start(&mut self) {
        let local_self = self.inner.clone();
        std::thread::spawn(move || {
            local_self.lock().unwrap().handle_signal();
            loop {
                std::thread::sleep(std::time::Duration::from_secs(1));
                local_self.lock().unwrap().show();
            }
            // std::process::exit(0);
        });
    }
}

fn main() -> Result<(), std::io::Error> {
    // Subscribe to the SIGUSR1 signal
    let sigs = vec![SIGUSR1];
    let mut signals = SignalsInfo::<WithOrigin>::new(sigs)?;

    // Run the application in its own thread
    let mut status = StatusLoop::new();
    status.start();

    // Consume all the incoming signals (this happens in "normal" Rust thread)
    for info in &mut signals {
        if info.signal == SIGUSR1 {
            status.inner.lock().unwrap().handle_signal();
        } else {
            println!("Terminating");
            break;
        }
    }

    Ok(())
}
